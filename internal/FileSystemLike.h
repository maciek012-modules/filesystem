#pragma once

#include <platform/platform.h>

#include <platform/filesystem/internal/FileBase.h>
#include <platform/filesystem/internal/FileSystemHandle.h>

namespace platform {

class FileSystemLike : public FileSystemHandle, public FileBase {
public:
    FileSystemLike(const char* name = NULL) : FileBase(name, FileSystemPathType) {}
    virtual ~FileSystemLike() {}
};

}  // namespace platform