#pragma once
#include <stdint.h>

#include <platform/platform.h>
#include <sys/stat.h>

#define O_RDONLY   0       ///< Open for reading
#define O_WRONLY   1       ///< Open for writing
#define O_RDWR     2       ///< Open for reading and writing
#define O_NONBLOCK 0x0004  ///< Non-blocking mode
#define O_APPEND   0x0008  ///< Set file offset to end of file prior to each write
#define O_CREAT    0x0200  ///< Create file if it does not exist
#define O_TRUNC    0x0400  ///< Truncate file to zero length
#define O_EXCL     0x0800  ///< Fail if file exists
#define O_BINARY   0x8000  ///< Open file in binary mode

#define O_ACCMODE (O_RDONLY | O_WRONLY | O_RDWR)

#define NAME_MAX 255  ///< Maximum size of a name in a file path

namespace platform {



struct dirent {
    char    d_name[NAME_MAX + 1];  ///< Name of file
    uint8_t d_type;                ///< Type of file
};

class DirHandle : private NonCopyable {
public:
    virtual ~DirHandle() {}

    /**
     * @brief Read the next directory entry
     *
     * @param ent      The directory entry to fill out
     * @return         1 on reading a filename, 0 at end of directory, negative error on failure
     */
    virtual ssize_t read(struct dirent* ent) = 0;

    /**
     * @brief Close a directory
     *
     * @return          0 on success, negative error code on failure
     */
    virtual int close() = 0;

    /**
     * @brief Set the current position of the directory
     *
     * @param offset   Offset of the location to seek to, must be a value returned from tell
     */
    virtual void seek(off_t offset) = 0;

    /**
     * @brief Get the current position of the directory
     *
     * @return         Position of the directory that can be passed to rewind
     */
    virtual off_t tell() = 0;

    /**
     * @brief Rewind the current position to the beginning of the directory
     */
    virtual void rewind() = 0;

    /**
     * @brief Get the sizeof the directory
     *
     * @return         Number of files in the directory
     */
    virtual size_t size() {
        off_t          off  = tell();
        size_t         size = 0;
        struct dirent* ent  = new struct dirent;

        rewind();
        while (read(ent) > 0) {
            size += 1;
        }
        seek(off);

        delete ent;
        return size;
    }
};

}  // namespace platform