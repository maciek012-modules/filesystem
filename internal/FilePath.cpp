#include <platform/filesystem/internal/FilePath.h>

namespace platform {

FilePath::FilePath(const char* file_path) : file_name(NULL), fb(NULL) {
    // skip slashes
    file_path += strspn(file_path, "/");

    const char* file_system = file_path;
    file_name               = file_system;
    int len                 = 0;
    while (true) {
        char c = *file_name;
        if (c == '/') {   // end of object name
            file_name++;  // point to one char after the '/'
            break;
        }
        if (c == 0) {  // end of object name, with no filename
            break;
        }
        len++;
        file_name++;
    }

    fb = FileBase::lookup(file_system, len);
}

const char* FilePath::fileName(void) {
    return file_name;
}

bool FilePath::isFileSystem(void) {
    if (NULL == fb) {
        return false;
    }
    return (fb->getPathType() == FileSystemPathType);
}

FileSystemLike* FilePath::fileSystem(void) {
    if (isFileSystem()) {
        return static_cast<FileSystemLike*>(fb);
    }
    return NULL;
}

bool FilePath::isFile(void) {
    if (NULL == fb) {
        return false;
    }
    return (fb->getPathType() == FilePathType);
}

FileLike* FilePath::file(void) {
    if (isFile()) {
        return (FileLike*)fb;
    }
    return NULL;
}

bool FilePath::exists(void) {
    return fb != NULL;
}

}  // namespace platform
