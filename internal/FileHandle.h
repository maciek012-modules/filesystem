#pragma once

typedef int FILEHANDLE;

#include <cstdio>

#include <platform/platform.h>

namespace platform {
/**
 * @class FileHandle
 *
 * @brief An abstract interface that represents operations on a file-like object. The core functions
 * are read, write and seek, but only a subset of these operations can be provided.
 *
 * @note to create a file, @see File
 * @note Synchronization level: Set by subclass
 */
class FileHandle : private NonCopyable {
public:
    virtual ~FileHandle() = default;

    /**
     * @brief Read the contents of a file into a buffer
     *
     * Devices acting as FileHandles should follow POSIX semantics:
     *
     * * if no data is available, and nonblocking set, return -EAGAIN
     * * if no data is available, and blocking set, wait until some data is available
     * * If any data is available, call returns immediately
     *
     * @param buffer   The buffer to read in to
     * @param size     The number of bytes to read
     * @return         The number of bytes read, 0 at end of file, negative error on failure
     */
    virtual ssize_t read(void* buffer, size_t size) = 0;

    /**
     * @brief Write the contents of a buffer to a file
     *
     * Devices acting as FileHandles should follow POSIX semantics:
     *
     * - if blocking, block until all data is written
     * - if no data can be written, and nonblocking set, return -EAGAIN
     * - if some data can be written, and nonblocking set, write partial
     *
     * @param buffer   The buffer to write from
     * @param size     The number of bytes to write
     * @return         The number of bytes written, negative error on failure
     */
    virtual ssize_t write(const void* buffer, size_t size) = 0;

    /**
     * @brief Move the file position to a given offset from from a given location
     *
     * @param offset The offset from whence to move to
     * @param whence The start of where to seek SEEK_SET to start from beginning of file, SEEK_CUR
     * to start from current position in file, SEEK_END to start from end of file
     * @return         The new offset of the file, negative error code on failure
     */
    virtual off_t seek(off_t offset, int whence = SEEK_SET) = 0;

    /**
     * @brief Close a file
     *
     * @return         0 on success, negative error code on failure
     */
    virtual int close() = 0;

    /**
     * @brief Flush any buffers associated with the file
     *
     * @return         0 on success, negative error code on failure
     */
    virtual int sync() { return 0; }

    /**
     * @brief Check if the file in an interactive terminal device
     *
     * @return         True if the file is a terminal
     * @return         False if the file is not a terminal
     * @return         Negative error code on failure
     */
    virtual int isatty() { return false; }

    /**
     * @brief Get the file position of the file
     *
     * @note This is equivalent to seek(0, SEEK_CUR)
     *
     * @return         The current offset in the file, negative error code on failure
     */
    virtual off_t tell() { return seek(0, SEEK_CUR); }

    /**
     * @brief Rewind the file position to the beginning of the file
     *
     * @note This is equivalent to seek(0, SEEK_SET)
     */
    virtual void rewind() { seek(0, SEEK_SET); }

    /**
     * @brief Get the size of the file
     *
     * @return         Size of the file in bytes
     */
    virtual off_t size();

    /**
     * @brief Truncate or extend a file.
     *
     * The file's length is set to the specified value. The seek pointer is not changed. If the file
     * is extended, the extended area appears as if it were zero-filled.
     *
     * @param length   The requested new length for the file
     *
     * @return         Zero on success, negative error code on failure
     */
    virtual int truncate(off_t length) { return -EINVAL; }

    /**
     * @brief Set blocking or nonblocking mode of the file operation like read/write.  Definition
     * depends on the subclass implementing FileHandle.  The default is blocking.
     *
     * @param blocking     true for blocking mode, false for nonblocking mode.
     *
     * @return             0 on success
     * @return             Negative error code on failure
     */
    virtual int set_blocking(bool blocking) { return blocking ? 0 : -ENOTTY; }

    /**
     * @brief Check current blocking or nonblocking mode for file operations.
     *
     * @return             true for blocking mode, false for nonblocking mode.
     */
    virtual bool is_blocking() const { return true; }

    /**
     * @brief Enable or disable input
     *
     * Control enabling of device for input. This is primarily intended for temporary power-saving;
     * the overall ability of the device to operate for input and/or output may be fixed at creation
     * time, but this call can allow input to be temporarily disabled to permit power saving without
     * losing device state.
     *
     * @param enabled      true to enable input, false to disable.
     *
     * @return             0 on success
     * @return             Negative error code on failure
     */
    virtual int enable_input(bool enabled) { return -EINVAL; }

    /**
     * @brief Control enabling of device for output. This is primarily intended for temporary
     * power-saving; the overall ability of the device to operate for input and/or output may be
     * fixed at creation time, but this call can allow output to be temporarily disabled to permit
     * power saving without losing device state.
     *
     * @param enabled      true to enable output, false to disable.
     *
     * @return             0 on success
     * @return             Negative error code on failure
     */
    virtual int enable_output(bool enabled) { return -EINVAL; }

    /**
     * @brief Check for poll event flags. You can use or ignore the input parameter. You can return
     * all events or check just the events listed in events.  Call is nonblocking - returns
     * instantaneous state of events.  Whenever an event occurs, the derived class should call the
     * sigio() callback).
     *
     * @param events        bitmask of poll events we're interested in - POLLIN/POLLOUT etc.
     *
     * @returns             bitmask of poll events that have occurred.
     */
    // virtual short poll(short events) const {
    //     // Possible default for real files
    //     return POLLIN | POLLOUT;
    // }

    /**
     * @brief Definition depends on the subclass implementing FileHandle.  For example, if the
     * FileHandle is of type Stream, writable() could return true when there is ample buffer space
     * available for write() calls.
     *
     * @returns             true if the FileHandle is writable.
     */
    // bool writable() const { return poll(POLLOUT) & POLLOUT; }

    /**
     * @brief Definition depends on the subclass implementing FileHandle.  For example, if the
     * FileHandle is of type Stream, readable() could return true when there is something available
     * to read.
     *
     * @returns            true when there is something available to read.
     */
    // bool readable() const { return poll(POLLIN) & POLLIN; }
};

}  // namespace platform