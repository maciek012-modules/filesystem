#pragma once

#include <cstring>
#include <platform/filesystem/internal/FileSystemLike.h>
#include <platform/filesystem/internal/FileLike.h>
namespace platform {

class FileSystem;

class FilePath {
public:
    FilePath(const char* file_path);

    const char* fileName(void);

    bool            isFileSystem(void);
    FileSystemLike* fileSystem(void);

    bool      isFile(void);
    FileLike* file(void);
    bool      exists(void);

private:
    const char* file_name;
    FileBase*   fb;
};

}  // namespace platform