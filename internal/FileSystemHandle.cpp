#include <platform/filesystem/internal/FileSystemHandle.h>

namespace platform {

int FileSystemHandle::open(platform::DirHandle** dir, const char* path) {
    return -ENOSYS;
}

int FileSystemHandle::remove(const char* path) {
    return -ENOSYS;
}

int FileSystemHandle::rename(const char* path, const char* newpath) {
    return -ENOSYS;
}

int FileSystemHandle::stat(const char* path, struct stat* st) {
    return -ENOSYS;
}

int FileSystemHandle::mkdir(const char* path, mode_t mode) {
    return -ENOSYS;
}

int FileSystemHandle::statvfs(const char* path, struct statvfs* buf) {
    return -ENOSYS;
}

}  // namespace platform