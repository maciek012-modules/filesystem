#pragma once

#include <platform/platform.h>

#include <platform/filesystem/internal/FileBase.h>
#include <platform/filesystem/internal/FileHandle.h>

namespace platform {

class FileLike : private NonCopyable, public FileHandle, public FileBase {
public:
    FileLike(const char* name = NULL) : FileBase(name, FilePathType) {}
    virtual ~FileLike() {}
};

}  // namespace platform