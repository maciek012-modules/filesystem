#pragma once

#include <platform/platform.h>

#include <platform/filesystem/internal/FileHandle.h>
#include <platform/filesystem/internal/DirHandle.h>

namespace platform {
/**
 * @class FileSystemHandle
 * @brief A filesystem-like object is one that can be used to open file-like objects though it by
 * fopen("/name/filename", mode). Implementations must define at least open (the default definitions
 * of the rest of the functions just return error values).
 *
 * @note Synchronization level: Set by subclass
 */
class FileSystemHandle : private NonCopyable {
public:
    /**
     * @brief FileSystemHandle lifetime
     */
    virtual ~FileSystemHandle() {}

    /** @brief Open a file on the filesystem
     *
     * @param file     Destination for the handle to a newly created file
     * @param filename The name of the file to open
     * @param flags    The flags to open the file in, one of O_RDONLY, O_WRONLY, O_RDWR, bitwise
     * or'd with one of O_CREAT, O_TRUNC, O_APPEND
     * @return         0 on success, negative error code on failure
     */
    virtual int open(FileHandle** file, const char* filename, int flags) = 0;

    /**
     * @brief Open a directory on the filesystem
     *
     * @param dir      Destination for the handle to the directory
     * @param path     Name of the directory to open
     * @return         0 on success, negative error code on failure
     */
    virtual int open(DirHandle** dir, const char* path);

    /**
     * @brief Remove a file from the filesystem.
     *
     * @param path     The name of the file to remove.
     * @return         0 on success, negative error code on failure
     */
    virtual int remove(const char* path);

    /**
     * @brief Rename a file in the filesystem.
     *
     * @param path     The name of the file to rename.
     * @param newpath  The name to rename it to
     * @return         0 on success, negative error code on failure
     */
    virtual int rename(const char* path, const char* newpath);

    /**
     * @brief Store information about the file in a stat structure
     *
     * @param path     The name of the file to find information about
     * @param st       The stat buffer to write to
     * @return         0 on success, negative error code on failure
     */
    virtual int stat(const char* path, struct stat* st);

    /**
     * @brief Create a directory in the filesystem.
     *
     * @param path     The name of the directory to create.
     * @param mode     The permissions with which to create the directory
     * @return         0 on success, negative error code on failure
     */
    virtual int mkdir(const char* path, mode_t mode);

    /**
     * @brief Store information about the mounted filesystem in a statvfs structure
     *
     * @param path     The name of the file to find information about
     * @param buf      The stat buffer to write to
     * @return         0 on success, negative error code on failure
     */
    virtual int statvfs(const char* path, struct statvfs* buf);
};

}  // namespace platform