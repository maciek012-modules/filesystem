#pragma once
typedef int FILEHANDLE;

#include <platform/platform.h>

namespace platform {



typedef enum {
    FilePathType,
    FileSystemPathType,
} PathType;

class FileBase : private NonCopyable {
public:
    FileBase(const char* name, PathType t);
    virtual ~FileBase();

    const char* getName(void);
    PathType    getPathType(void);

    static FileBase* lookup(const char* name, unsigned int len);

    static FileBase* get(int n);

    void set_as_default();

private:
    static inline FileBase* _head    = NULL;
    static inline FileBase* _default = NULL;

    FileBase*         _next;
    const char* const _name;
    const PathType    _path_type;
};

}  // namespace platform