#include <platform/filesystem/internal/FileHandle.h>

namespace platform {


off_t FileHandle::size()
{
    /* remember our current position */
    off_t off = seek(0, SEEK_CUR);
    if (off < 0) {
        return off;
    }
    /* seek to the end to get the file length */
    off_t size = seek(0, SEEK_END);
    /* return to our old position */
    seek(off, SEEK_SET);
    return size;
}

} // namespace platform